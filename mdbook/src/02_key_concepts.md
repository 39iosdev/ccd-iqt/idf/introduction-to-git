# Key Concepts

### **Commit**

Whenever you save your changes in one or more files, you can create a new commit in Git. A commit is like a snapshot of your entire repository at that point in time, not just of one or two files.

### **Commit Message**

Every time you make a commit, you need to supply a message that describes _why_ the change was made. That commit message is invaluable when trying to understand later why a certain change was implemented.

### **Stage**

Staging means that you have pending updates that git is aware of, but the changes have not been made permanent in the local repository.

### **Branch**

A branch is an independent series of commits off to one side that you can use to try out an experiment or create a new feature.

### **Main Branch**

Whenever you create a new Git project, there is a default branch created called `Main`. This is the branch that your work should end up on eventually, once it’s ready to push to production.

### **Merge**

A merge is a way to take completed work from one branch and incorporate it into another branch.

### **Tag**

A tag is a reference to a specific historic commit. Tags are most often used to document production releases so you know exactly which versions of the code went into production and when.

### **Merge Request**

Merge requests are used as code reviews before code from aV feature branch is to be added into the main project
branch.  Requests can be used after a branch has been created and the changes made, but the typically use is to
tie a merge request to an issue, so that when the issue has been worked and completed, there is a merge request already associated with it.

### **Clone**

Often you’ll want to download a copy of a project from GitLab so you can work on it locally.

### **Fork**

Sometimes you don’t have the necessary permission to make changes directly to a project.  If you want to submit changes to such a project, first you need to make a copy of the project under *your* user account on GitLab. That process is called *forking* the repository. You can then clone it, make changes, and submit them back to the original project using a pull request.
