# ![git logo](./assets/logo@2x.png)

* Introduction
* Key Concepts
* Download Git
* Creating a Repository
* Cloning and Branching
* Version Control
* CI/CD
* Configuration Files

Git Reference:

[https://git-scm.com/docs](https://git-scm.com/docs)

## To access the Git slides please click [here](./slides)
